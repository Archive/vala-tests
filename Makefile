VALAC = valac

all: testsuite

check: testsuite
	PATH=$$PATH:`pwd`/src VALAC="$(VALAC)" gtester --verbose --g-fatal-warnings -k -o test-report.xml testsuite
	gtester-report test-report.xml > test-report.html

testsuite: src/testsuite.vala src/testcase.vala src/posix.vapi
	$(VALAC) -o $@ $^

install: check
	true
