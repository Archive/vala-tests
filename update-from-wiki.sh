#!/bin/bash

PAGES="
AdvancedSample
BasicSample
CairoSample
CharacterSample
ClutterSamples
CouchDBSample
CursesSample
CustomWidgetSamples
DBusClientSamples
DBusClientSamples/Waiting
DBusServerSample
GConfSample
GIOCompressionSample
GIONetworkingSample
GIOSamples
GSLSample
GStreamerSample
GTKSample
GtkCellRendererSample
GdlSample
GeeSamples
GnomeDesktopAndGMenuExample
HildonSample
InputSamples
IoChannelsSample
JsonSample
LibSoupSample
ListSample
LoudmouthSample
LuaSample
MarkupSample
MxSample
OpenGLSamples
PanelAppletSample
PangoCairoSample
PopplerSample
PostgreSQL
PreprocessorSample
PropertiesSample
SDLSample
SignalsAndCallbacks
SqliteSample
StringSample
TestSample
ThreadingSamples
TiffSample
TimeSample
TypeModules
ValueSample
WebKitSample
XmlSample
ZLib
"

if [ "$#" != "0" ]; then
  PAGES="$@"
fi

for page in $PAGES ; do
  url="http://live.gnome.org/Vala/$page"
  pageraw=`mktemp`
  curl -s "$url?action=raw" -A "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)" > "$pageraw"

  # awk '/^}}}/{close(f); f=""} f{print > f} /^{{{#/{ f=gensub(/.*vala-test:([\w]*)$/, "tests/\\1", "g"); }' $pageraw

  IFS="
"
  while read -r line ; do
      line=$(echo "$line" | tr -d '\r')
      if [[ "$line" =~ vala-test:([-a-zA-Z0-9\.\/]*) ]] ; then
	  file="tests/${BASH_REMATCH[1]}"
	  echo "Updating $file..."
	  echo "
// $url vala-test:${BASH_REMATCH[1]}
" > "$file"
      elif [[ "$line" =~ '{{{' ]] ; then
          continue
      elif [[ "$line" =~ '}}}' ]] ; then
	  file=
      elif [[ "$file" != "" ]] ; then
	  echo "$line" >> "$file"
      fi
  done < "$pageraw"

  rm "$pageraw"
  sleep 20 # don't overload live.gnome.org...
done
