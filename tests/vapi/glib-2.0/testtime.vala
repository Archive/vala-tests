using GLib;

public class Testtime : GLib.Object {

    private static Time create_time (int year, int month, int day, int hour,
            int minute, int seconds) {
        var t = Time.gm (time_t ());
        
        t.year = year - 1900;
        t.month = month - 1;
        t.day = day;
        t.hour = hour;
        t.minute = minute;
        t.second = seconds;
        
        return t;
    }
    
    public static void main (string[] args) {
        Time t1 = create_time (2008, 10, 17, 21, 05, 34);
        //FIXME
        //stdout.printf (t1.to_string ());
        
        Time t2 = create_time (2008, 10, 17, 23, 11, 59);
        //FIXME
        //stdout.printf (t2.to_string ());
        
        time_t tt1 = t1.mktime ();
        Time nt1 = Time.local (tt1);
        time_t ntt1 = nt1.mktime ();
        assert (ntt1 == tt1);
        
        time_t tt2 = t2.mktime ();
        Time nt2 = Time.local (tt2);
        time_t ntt2 = nt2.mktime ();
        assert (ntt2 == tt2);
        
        t2.minute += 62;
        t2.second += 342;
        time_t ntt3 = t2.mktime ();
        assert (ntt3 - ntt2 == 62*60 + 342);
    }

}
