using GLib;

public class Filetest : GLib.Object {

	public static const string FILENAME = "testfile.txt";
	private const int BUFFER_SIZE = 4096;
	public static const string CONTENT =
"""
This is
     a test
string
to check
  if some-
thing went wrong.
""";

	public static void write_file (File f) {
		FileOutputStream fostream;
		try {
			fostream = f.create (0, null);
		} catch (Error e) {
			critical ("Could not create file: %s", e.message);
			return;
		}
		OutputStream ostream = new BufferedOutputStream (fostream);

		try {
			ostream.write (CONTENT, CONTENT.size (), null);
		} catch (Error e) {
			critical ("Could not write to stream: %s", e.message);
			return;
		}
		
		try {
			ostream.close (null);
		} catch (Error e) {
			critical ("Could not close stream: %s", e.message);
			return;
		}
	}

	public static string? read_file (File f) {
		FileInputStream stream;
        try {
            stream = f.read (null);
        } catch (IOError e) {
            critical ("Could not open file: %s", e.message);
            return null;
        }
    
        StringBuilder sb = new StringBuilder ();               
        char[] buffer = new char[BUFFER_SIZE];
        
        try {
		    long bytes_read;
		    while ((bytes_read = stream.read (buffer, BUFFER_SIZE, null)) > 0) {
		        for (int i=0; i<bytes_read; i++) {
		            sb.append_c (buffer[i]);
		        }
		    }
		} catch (Error e) {
			critical ("Could not read file: %s", e.message);
            return null;
		}
		
	
		try {	
	        stream.close (null);
        } catch (Error e) {
			critical ("Could not close stream: %s", e.message);
			return null;
		}
		
        return sb.str;
	}

	public static void main (string[] args) {
		File f = File.new_for_path (FILENAME);
		
		write_file (f);
		
		assert (f.query_exists (null));
		
		string? readtxt = read_file (f);
		
		assert (readtxt != null && readtxt == CONTENT);
	}

}
