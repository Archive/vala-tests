
// http://live.gnome.org/Vala/PanelAppletSample vala-test:examples/panel-applet-simple.vala

using Panel;

public class MainApplet : GLib.Object {

    public static bool panel_factory (Applet applet, string iid) {
        var button = new Gtk.Button.with_label ("Vala Panel");
        applet.add (button);
        applet.show_all ();
        return false;
    }

    public static int main (string[] args) {
        var program = Gnome.Program.init ("Vala_Applet", "0", Gnome.libgnomeui_module,
                                          args, "sm-connect", false);
        return Applet.factory_main ("OAFIID:Vala_Applet_Factory",
                                    typeof (Panel.Applet),
                                    MainApplet.panel_factory);
    }
}
