
// http://live.gnome.org/Vala/DBusServerSample vala-test:examples/dbus-demo-server.vala

[DBus (name = "org.example.Demo")]
public class DemoServer : Object {
    private int counter;

    public int ping (string msg) {
        stdout.printf ("%s\n", msg);
        return counter++;
    }
}

void main () {
    try {
        var conn = DBus.Bus.get (DBus.BusType.SESSION);
        dynamic DBus.Object bus = conn.get_object ("org.freedesktop.DBus",
                                                   "/org/freedesktop/DBus",
                                                   "org.freedesktop.DBus");

        // try to register service in session bus
        uint reply = bus.request_name ("org.example.Demo", (uint) 0);
        assert (reply == DBus.RequestNameReply.PRIMARY_OWNER);

        var demo = new DemoServer ();
        conn.register_object ("/org/example/demo", demo);

        var loop = new MainLoop ();
        loop.run ();

    } catch (DBus.Error e) {
        stderr.printf ("%s\n", e.message);
    }
}
