
// http://live.gnome.org/Vala/ZLib vala-test:examples/zlib.vala

/*
 * Deflate and inflate a file in gzip format
 */

using ZLib;

class ZLibTest : Object {
    // 16 KB buffer size
    private const int CHUNK = 16 * 1024;

    public int inflate (InputStream source, OutputStream dest) throws Error {
        var buf_in = new uchar[CHUNK];
        var buf_out = new uchar[CHUNK];
        int ret = Status.OK;
        var strm = InflateStream.full (15 | 32);   // no way to check if this failed...

        // decompress until stream ends or end of file
        do {
            strm.avail_in = (int) source.read (buf_in, CHUNK, null);
            if (strm.avail_in == 0) {
                print ("Inflate: strm not avail\n");
                break;
            }
            strm.next_in = buf_in;

            // run inflate() on input until output buffer not full
            do {
                strm.avail_out = buf_out.length;
                strm.next_out = buf_out;

                ret = strm.inflate (Flush.NO_FLUSH);
                assert (ret != Status.STREAM_ERROR);  // state not clobbered
                if (ret == Status.NEED_DICT) {
                    ret = Status.DATA_ERROR;      // and fall through
                }
                switch (ret) {
                case Status.DATA_ERROR:
                case Status.MEM_ERROR:
                    return ret;
                }

                uint have = CHUNK - strm.avail_out;
                if (dest.write (buf_out, have, null) != have) {
                    return Status.ERRNO;
                }
            } while (strm.avail_out == 0);

            // done when inflate () says it's done
        } while (ret != Status.STREAM_END);

        if (ret == Status.STREAM_END) {
            print ("Inflate: Status.OK\n");
        } else {
            print ("Inflate: Status.DATA_ERROR\n");
        }

        return ret == Status.STREAM_END ? Status.OK : Status.DATA_ERROR;
    }

    public int deflate (InputStream source, OutputStream dest) throws Error {
        var buf_in = new uchar[CHUNK];
        var buf_out = new uchar[CHUNK];
        int flush = Flush.NO_FLUSH;
        int ret = Status.OK;
        var strm = DeflateStream.full (Level.BEST_COMPRESSION,
                                       Algorithm.DEFLATED,
                                       (15 + 16), // + 16 for gzip output format
                                       8, Strategy.DEFAULT_STRATEGY);

        // compress until deflate stream ends or end of file
        do {
            strm.avail_in = (int) source.read (buf_in, CHUNK, null);

            if (strm.avail_in == 0) {
                flush = Flush.FINISH;
            } else {
                flush = Flush.NO_FLUSH;
            }

            strm.next_in = buf_in;

            // run deflate() on input until output buffer not full
            do {
                strm.avail_out = buf_out.length;
                strm.next_out = buf_out;

                ret = strm.deflate (flush);
                assert (ret != Status.STREAM_ERROR);  // state not clobbered
                uint have = CHUNK - strm.avail_out;
                if (dest.write (buf_out, have, null) != have) {
                    print ("write error\n");
                    return Status.ERRNO;
                }
            } while (strm.avail_out == 0);
            assert (strm.avail_in == 0);
        } while (flush != Flush.FINISH); 

        if (ret == Status.STREAM_END) {
            print ("Deflate: Status.OK\n");
        } else {
            print ("Deflate: Status.DATA_ERROR\n");
        }

        return (ret == Status.STREAM_END) ? Status.OK : Status.DATA_ERROR;
    }
}


int main (string[] args) {
    if (args.length <= 1) {
        print ("usage: zlibexample FILE\n");
        return 0;
    }

    var infile = File.new_for_commandline_arg (args[1]);
    if (!infile.query_exists (null)) {
        print ("FILE does not exist.\n");
        return 0;
    }

    var zippedfile = File.new_for_commandline_arg (args[1] + ".gz");
    var outfile = File.new_for_commandline_arg (args[1] + "_out");

    var zlib_test = new ZLibTest ();

    try {
        var instream = infile.read (null);
        if (zippedfile.query_exists (null)) {
            zippedfile.delete (null);
        }
        var zipstream = zippedfile.create (FileCreateFlags.NONE, null);
        zlib_test.deflate (instream, zipstream);
    } catch (Error e) {
        stderr.printf ("Deflate error: %s\n", e.message);
        return 1;
    }

    try {
        var zipinstream = zippedfile.read (null);
        if (outfile.query_exists (null)) {
            outfile.delete (null);
        }
        var outstream = outfile.create (FileCreateFlags.NONE, null);
        zlib_test.inflate (zipinstream, outstream);
    } catch (Error e) {
        stderr.printf ("Inflate error: %s\n", e.message);
        return 1;
    }

    return 0;
}
