
// http://live.gnome.org/Vala/GIOSamples vala-test:examples/gio-ls.vala

int main (string[] args) {
    try {
        var directory = File.new_for_path (".");

        if (args.length > 1) {
            directory = File.new_for_commandline_arg (args[1]);
        }

        var enumerator = directory.enumerate_children (FILE_ATTRIBUTE_STANDARD_NAME, 0, null);

        FileInfo file_info;
        while ((file_info = enumerator.next_file (null)) != null) {
            stdout.printf ("%s\n", file_info.get_name ());
        }

    } catch (Error e) {
        stderr.printf ("Error: %s\n", e.message);
        return 1;
    }

    return 0;
}
