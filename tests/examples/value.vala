
// http://live.gnome.org/Vala/ValueSample vala-test:examples/value.vala

void main () {
    Value v;  // a GLib.Value

    v = 5;  // an integer auto-boxed into a Value
    print ("value: %d\n", (int) v);  // unboxing via cast

    // reset to its default value
    v.reset ();
    print ("value: %d\n", (int) v);  // unboxing via cast

    v = "hello";  // a string auto-boxed into a Value
    print ("value: %s\n", (string) v);  // unboxing via cast
}
