
// http://live.gnome.org/Vala/DBusClientSamples vala-test:examples/dbus-skype.vala

[DBus (name = "com.Skype.API")]
interface Skype : Object {
    public abstract string invoke (string cmd) throws DBus.Error;
}

string send (Skype skype, string cmd) throws DBus.Error {
    return skype.invoke (cmd);
}

void send_check (Skype skype, string cmd, string expected) throws DBus.Error {
    string actual = send (skype, cmd);
    if (actual != expected) {
        stderr.printf ("Bad result '%s', expected '%s'\n", actual, expected);
    }
}

int main (string[] args) {
    try {
        var conn = DBus.Bus.get (DBus.BusType.SESSION);
        var skype = (Skype) conn.get_object ("com.Skype.API", "/com/Skype");

        send_check (skype, "NAME skype-status-client", "OK");
        send_check (skype, "PROTOCOL 2", "PROTOCOL 2");

        // if no arguments given, show current status, otherwise update
        // status to first argument
        if (args.length < 2) {
            stdout.printf ("%s\n", send (skype, "GET USERSTATUS"));
        } else {
            // possible statuses: ONLINE OFFLINE SKYPEME AWAY NA DND INVISIBLE
            send_check (skype, "SET USERSTATUS " + args[1], "USERSTATUS " + args[1]);
        }

    } catch (DBus.Error e) {
        stderr.printf ("%s\n", e.message);
        return 1;
    }

    return 0;
}
