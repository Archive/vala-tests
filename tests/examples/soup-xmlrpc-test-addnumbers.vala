
// http://live.gnome.org/Vala/LibSoupSample vala-test:examples/soup-xmlrpc-test-addnumbers.vala

using Soup;

void main () {
    var message = xmlrpc_request_new ("http://kushaldas.wordpress.com/xmlrpc.php",
                                     "demo.addTwoNumbers",
                                     typeof (int), 20,
                                     typeof (int), 30);
    var session = new SessionSync ();
    session.send_message (message);
    
    string data = message.response_body.flatten ().data;
    try {
        Value v = Value (typeof (int));
        xmlrpc_parse_method_response (data, -1, v);
        stdout.printf ("Result: %d\n", (int) v);
    } catch (Error e) {
        stderr.printf ("Error while processing the response: %s\n", e.message);
    }
}
