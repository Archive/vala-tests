
// http://live.gnome.org/Vala/JsonSample vala-test:examples/json-google-translator.vala

string translate (string text, string input_language, string output_language) throws Error {

    string uri = "http://ajax.googleapis.com/ajax/services/language/translate";
    string version = "1.0";
    string full_uri = "%s?v=%s&q=%s&langpair=%s|%s".printf (uri, version,
                                 Soup.URI.encode (text, null),
                                 input_language, output_language);

    var session = new Soup.SessionAsync ();
    var message = new Soup.Message ("GET", full_uri);
    session.send_message (message);

    var parser = new Json.Parser ();
    parser.load_from_data (message.response_body.flatten ().data, -1);

    var root_object = parser.get_root ().get_object ();
    string translated_text = root_object.get_object_member ("responseData")
                                        .get_string_member ("translatedText");

    /* Here you can get the value of responseDetails */
//  int64 response_details = root_object.get_int_member ("responseDetails");
//  stdout.printf ("Response Details : %lld\n", response_details);

    /* Here you can get the value of responseStatus */
//  int64 response_status = root_object.get_int_member ("responseStatus");
//  stdout.printf ("Response Status  : %lld\n", response_status);

    return translated_text;
}

void main () {
    try {
        string original_text = "Hello World";
        string translated_text = translate (original_text, "en", "ar");
        stdout.printf ("Translated text: %s\n", translated_text);
    } catch (Error e) {
        stderr.printf ("I think something went wrong!\n");
    }
}
