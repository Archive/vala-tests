
// http://live.gnome.org/Vala/PangoCairoSample vala-test:examples/pango-cairo.vala

//======================================================================
//  pags.vala - Converting text to rendered image files of the text.
//              It may e.g. be used for rendering text files to put
//              into a mp3 player/image viewer.
//
//  Dov Grobgeld <dov.grobgeld@gmail.com>
//  Sat Jun 28 22:50:57 2008
//----------------------------------------------------------------------

using Cairo;

public class Pags {

    static void clear_page(Cairo.Context cr, int width, int height,
                           double margin, double top_margin, bool do_rotate)
    {
        cr.identity_matrix();
        cr.set_source_rgb (1.0, 1.0, 1.0); 
        cr.rectangle(0, 0, width, height);
        cr.fill();
        cr.set_source_rgb (0.0, 0.0, 0.0);
        if (do_rotate) {
            cr.translate(width, 0);
            cr.rotate(Math.PI / 2);
        }
        cr.move_to(margin, top_margin);
    }

    const OptionEntry[] option_entries = {
        { "width", 'w', 0, OptionArg.INT, ref width, "Image width", "PIXELS" },
        { "height", 'h', 0, OptionArg.INT, ref height, "Image height", "PIXELS" },
        { "family", 'f', 0, OptionArg.STRING, ref font_family, "Font family", "NAME" },
        { "font-size", 's', 0, OptionArg.DOUBLE, ref font_size, "Font size", "VALUE" },
        { "margin", 'm', 0, OptionArg.DOUBLE, ref margin, "Margin", "VALUE" },
        { "justify", 'j', 0, OptionArg.NONE, ref do_justify, "Justify", null },
        { "rotate", 'r', 0, OptionArg.NONE, ref do_rotate, "Rotate", null },
        { "", 0, 0, OptionArg.FILENAME_ARRAY, ref filenames, null, "FILE" },
        { null }
    };

    static int width = 320;
    static int height = 240;
    static string font_family;
    static double font_size;
    static double margin;
    static bool do_justify = false;
    static bool do_rotate = false;
    static string[] filenames;

    static int main(string[] args)
    {
        font_family = "Sans";
        font_size = 14;
        margin = font_size;

        try {
            var opt_context = new OptionContext("- Render text files to image files");
            opt_context.set_help_enabled(true);
            opt_context.add_main_entries(option_entries, "pags");
            opt_context.parse(ref args);
        } catch (OptionError e) {
            stderr.printf("Option parsing failed: %s\n", e.message);
            return -1;
        }

        double top_margin = margin * 2;
        double bottom_margin = margin;

        if (filenames == null) {
            stdout.printf("Need name of text file!\n");
            return -1;
        }

        Pango.Rectangle ink_rect, logical_rect;

        string text;
        try {
            FileUtils.get_contents(filenames[0], out text);
        } catch (FileError e) {
            stderr.printf("Failed reading file %s!\n", filenames[0]);
            return -1;
        }

        var surface = new Cairo.ImageSurface(Cairo.Format.RGB24, width, height);
        var cr = new Cairo.Context(surface);

        var font_description = new Pango.FontDescription();
        font_description.set_family(font_family);
        font_description.set_size((int)(font_size * Pango.SCALE));

        var rwidth = width;
        var rheight = height;
        if (do_rotate) {
            rwidth = height;
            rheight = width;
        }
        var layout = Pango.cairo_create_layout(cr);
        layout.set_font_description(font_description);
        layout.set_justify(do_justify);
        layout.set_width((int)((rwidth - 2 * margin) * Pango.SCALE));
        layout.set_text(text, -1);

        var pagenum_font_description = new Pango.FontDescription();
        pagenum_font_description.set_family("Sans");
        pagenum_font_description.set_size((int)(9 * Pango.SCALE));
        var pagenum_layout = Pango.cairo_create_layout(cr);
        pagenum_layout.set_font_description(pagenum_font_description);

        // tbd - move to the baseline pos of the first line
        int page_num = 1;
        double ybottom = rheight - top_margin - bottom_margin;
        unowned Pango.LayoutIter iter = layout.get_iter();

        clear_page(cr, width, height, margin, top_margin, do_rotate);
        while (!iter.at_last_line()) {
            double y_pos = 0;
            bool first_line = true;

            while (!iter.at_last_line()) {
                iter.get_line_extents(out ink_rect, out logical_rect);
                var line = iter.get_line_readonly();
                iter.next_line();

                // Decrease paragraph spacing
                if (ink_rect.width == 0) {
                    double dy = font_size / 2;
                    y_pos += dy;
                    if (!first_line)
                        cr.rel_move_to(0, dy);
                } else {
                    double xstart = 1.0 * logical_rect.x / Pango.SCALE;
                    cr.rel_move_to(xstart, 0);
                    Pango.cairo_show_layout_line(cr, line);
                    cr.rel_move_to(-xstart, (int)(logical_rect.height / Pango.SCALE));
                    y_pos += logical_rect.height / Pango.SCALE;
                }

                if (y_pos > ybottom)
                    break;
                first_line = false;
            }

            // draw page at bottom
            pagenum_layout.set_text(page_num.to_string(), -1);
            pagenum_layout.get_extents(out ink_rect, out logical_rect);
            cr.move_to(rwidth - logical_rect.width / Pango.SCALE, rheight - margin);
            Pango.cairo_show_layout(cr, pagenum_layout);

            surface.write_to_png("page-%03d.png".printf(page_num));
            page_num++;
            cr.show_page();
            clear_page(cr, width, height, margin, top_margin, do_rotate);
        }

        return 0;
    }
}
