
// http://live.gnome.org/Vala/CairoSample vala-test:examples/cairo-shaped.vala

using Gtk;
using Cairo;

/**
 * This example creates a clock with the following features:
 * Shaped window -- Window is unbordered and transparent outside the clock
 * Events are only registered for the window on the hour dots or on the center
 * dot. When the mouse is "in" the window (on one of the dots) it will turn
 * green.
 * This helps you understand where the events are actually being registered
 * Clicking allows you to drag the clock.
 * There is currently no code in place to close the window, you must kill the
 * process manually. A Composited environment is required. The python code I
 * copied this from includes checks for this. In my laziness I left them out.
 */
public class CairoShaped : Gtk.Window {

    // Are we inside the window?
    private bool inside = false;

    /**
     * Just creating the window, setting things up
     */
    public CairoShaped () {
        this.title = "Cairo Vala Demo";
        set_default_size (200, 200);

        // 'skip_taskbar_hint' determines whether the window gets an icon in
        // the taskbar / dock
        this.skip_taskbar_hint = true;

        // Turn off the border decoration
        this.decorated = false;
        this.app_paintable = true;

        // Need to get the RGBA colormap or transparency doesn't work.
        set_colormap (this.screen.get_rgba_colormap ());

        // We need to register which events we are interested in
        add_events (Gdk.EventMask.BUTTON_PRESS_MASK);
        add_events (Gdk.EventMask.ENTER_NOTIFY_MASK);
        add_events (Gdk.EventMask.LEAVE_NOTIFY_MASK);

        // Connecting some events, 'queue_draw()' redraws the window.
        // 'begin_move_drag()' sets up the window drag
        this.enter_notify_event.connect (() => {
            this.inside = true;
            queue_draw ();
            return true;
        });
        this.leave_notify_event.connect (() => {
            this.inside = false;
            queue_draw ();
            return true;
        });
        this.button_press_event.connect ((e) => {
            begin_move_drag ((int) e.button, (int) e.x_root, (int) e.y_root, e.time);
            return true;
        });

        // The expose event is what is called when we need to draw the window
        this.expose_event.connect (on_expose);

        this.destroy.connect (Gtk.main_quit);
    }

    /**
     * Actual drawing takes place within this method
     */
    private bool on_expose (Widget da, Gdk.EventExpose event) {
        // Get a cairo context for our window
        var ctx = Gdk.cairo_create (da.window);

        // This makes the current color transparent (a = 0.0)
        ctx.set_source_rgba (1.0, 1.0, 1.0, 0.0);

        // Paint the entire window transparent to start with.
        ctx.set_operator (Cairo.Operator.SOURCE);
        ctx.paint ();

        // If we wanted to allow scaling we could do some calculation here
        float radius = 100;

        // This creates a radial gradient. c() is just a helper method to
        // convert from 0 - 255 scale to 0.0 - 1.0 scale.
        var p = new Cairo.Pattern.radial (100, 100, 0, 100, 100, 100);
        if (inside) {
            p.add_color_stop_rgba (0.0, c (10), c (190), c (10), 1.0);
            p.add_color_stop_rgba (0.8, c (10), c (190), c (10), 0.7);
            p.add_color_stop_rgba (1.0, c (10), c (190), c (10), 0.5);
        } else {
            p.add_color_stop_rgba (0.0, c (10), c (10), c (190), 1.0);
            p.add_color_stop_rgba (0.8, c (10), c (10), c (190), 0.7);
            p.add_color_stop_rgba (1.0, c (10), c (10), c (190), 0.5);
        }

        // Set the gradient as our source and paint a circle.
        ctx.set_source (p);
        ctx.arc (100, 100, radius, 0, 2.0 * 3.14);
        ctx.fill ();
        ctx.stroke ();

        // This chooses the color for the hour dots
        if (inside) {
            ctx.set_source_rgba (0.0, 0.2, 0.6, 0.8);
        } else {
            ctx.set_source_rgba (c (226), c (119), c (214), 0.8);
        }

        // Draw the 12 hour dots.
        for (int i = 0; i < 12; i++) {
            ctx.arc (100 + 90.0 * Math.cos (2.0 * 3.14 * (i / 12.0)),
                     100 + 90.0 * Math.sin (2.0 * 3.14 * (i / 12.0)),
                     5, 0, 2.0 * 3.14);
            ctx.fill ();
            ctx.stroke ();
        }

        // This is the math to draw the hands.
        // Nothing overly useful in this section
        ctx.move_to (100, 100);
        ctx.set_source_rgba (0, 0, 0, 0.8);

        var t = Time.local (time_t ());
        int hour = t.hour;
        int minutes = t.minute;
        int seconds = t.second;
        double per_hour = (2 * 3.14) / 12;
        double dh = (hour * per_hour) + ((per_hour / 60) * minutes);
        dh += 2 * 3.14 / 4;
        ctx.set_line_width (0.05 * radius);
        ctx.rel_line_to (-0.5 * radius * Math.cos (dh), -0.5 * radius * Math.sin (dh));
        ctx.move_to (100, 100);
        double per_minute = (2 * 3.14) / 60;
        double dm = minutes * per_minute;
        dm += 2 * 3.14 / 4;
        ctx.rel_line_to (-0.9 * radius * Math.cos (dm), -0.9 * radius * Math.sin (dm));
        ctx.move_to (100, 100);
        double per_second = (2 * 3.14) / 60;
        double ds = seconds * per_second;
        ds += 2 * 3.14 / 4;
        ctx.rel_line_to (-0.9 * radius * Math.cos (ds), -0.9 * radius * Math.sin (ds));
        ctx.stroke ();

        // Drawing the center dot
        ctx.set_source_rgba (c (124), c (32), c (113), 0.7);

        ctx.arc (100, 100, 0.1 * radius, 0, 2.0 * 3.14);
        ctx.fill ();
        ctx.stroke ();

        // This is possibly the most important bit.
        // Here is where we create the mask to shape the window
        // And decide what areas will receive events and which areas
        // Will let events pass through to the windows below.

        // First create a pixmap the size of the window
        var px = new Gdk.Pixmap (null, 200, 200, 1);
        // Get a context for it
        var pmcr = Gdk.cairo_create (px);

        // Initially we want to blank out everything in transparent as we
        // Did initially on the ctx context
        pmcr.set_source_rgba (1.0, 1.0, 1.0, 0.0);
        pmcr.set_operator (Cairo.Operator.SOURCE);
        pmcr.paint ();

        // Now the areas that should receive events need to be made opaque
        pmcr.set_source_rgba (0, 0, 0, 1);

        // Here we copy the motions to draw the middle dots and the hour dots.
        // This is mostly to demonstrate that you can make this any shape you
        // want.
        pmcr.arc (100, 100, 10, 0, 2.0 * 3.14);
        pmcr.fill ();
        pmcr.stroke ();
        for (int i = 0; i < 12; i++) {
            pmcr.arc (100 + 90.0 * Math.cos (2.0 * 3.14 * (i / 12.0)),
                      100 + 90.0 * Math.sin (2.0 * 3.14 * (i / 12.0)),
                      5, 0, 2.0 * 3.14);
            pmcr.fill ();
            pmcr.stroke ();
        }

        // This sets the mask. Note that we have to cast to a Gdk.Bitmap*,
        // it won't compile without that bit.
        input_shape_combine_mask ((Gdk.Bitmap*) px, 0, 0);
        return true;
    }

    private double c (int val) {
        return val / 255.0;
    }

    static int main (string[] args) {
        Gtk.init (ref args);

        var cairo_sample = new CairoShaped ();
        cairo_sample.show_all ();

        // Just a timeout to update once a second.
        Timeout.add_seconds (1, () => {
            cairo_sample.queue_draw ();
            return true;
        });

        Gtk.main ();

        return 0;
    }
}
