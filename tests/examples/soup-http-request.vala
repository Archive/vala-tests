
// http://live.gnome.org/Vala/LibSoupSample vala-test:examples/soup-http-request.vala

void main () {

    var session = new Soup.SessionAsync ();
    var message = new Soup.Message ("GET", "http://adi.roiban.ro");

    /* see if we need HTTP auth */
    session.authenticate.connect ((sess, msg, auth, retrying) => {
        if (!retrying) {
            stdout.printf ("Authentication required\n");
            auth.authenticate ("user", "password");
        }
    });

    /* send a sync request */
    session.send_message (message);
    message.response_headers.foreach ((name, val) => {
        stdout.printf ("Name: %s -> Value: %s\n", name, val);
    });

    stdout.printf ("Message length: %lld\n%s\n",
                   message.response_body.length,
                   message.response_body.data);
}
