
// http://live.gnome.org/Vala/GStreamerSample vala-test:examples/gstreamer-audio-player.vala

using Gst;

public class StreamPlayer {

    private MainLoop loop = new MainLoop ();

    private void foreach_tag (Gst.TagList list, string tag) {
        switch (tag) {
        case "title":
            string tag_string;
            list.get_string (tag, out tag_string);
            stdout.printf ("tag: %s = %s\n", tag, tag_string);
            break;
        default:
            break;
        }
    }

    private bool bus_callback (Gst.Bus bus, Gst.Message message) {
        switch (message.type) {
        case MessageType.ERROR:
            GLib.Error err;
            string debug;
            message.parse_error (out err, out debug);
            stdout.printf ("Error: %s\n", err.message);
            loop.quit ();
            break;
        case MessageType.EOS:
            stdout.printf ("end of stream\n");
            break;
        case MessageType.STATE_CHANGED:
            Gst.State oldstate;
            Gst.State newstate;
            Gst.State pending;
            message.parse_state_changed (out oldstate, out newstate,
                                         out pending);
            stdout.printf ("state changed: %s->%s:%s\n",
                           oldstate.to_string (), newstate.to_string (),
                           pending.to_string ());
            break;
        case MessageType.TAG:
            Gst.TagList tag_list;
            stdout.printf ("taglist found\n");
            message.parse_tag (out tag_list);
            tag_list.foreach ((TagForeachFunc) foreach_tag);
            break;
        default:
            break;
        }

        return true;
    }

    public void play (string stream) {
        dynamic Element play = ElementFactory.make ("playbin", "play");
        play.uri = stream;

        Bus bus = play.get_bus ();
        bus.add_watch (bus_callback);

        play.set_state (State.PLAYING);

        loop.run ();
    }
}

const string DEFAULT_STREAM = "http://streamer-dtc-aa02.somafm.com:80/stream/1018";

int main (string[] args) {

    Gst.init (ref args);

    var player = new StreamPlayer ();
    player.play (args.length > 1 ? args[1] : DEFAULT_STREAM);

    return 0;
}
