
// http://live.gnome.org/Vala/GConfSample vala-test:examples/gconf.vala

/*
 * Various operations with GConf: getting, setting, unsetting and notifying
 */

using GConf;

public class GConfSample {

    public void run () throws GLib.Error {

        // You don't create a GConf.Client with "new GConf.Client ()" but
        // rather this way:
        var gc = GConf.Client.get_default ();

        // A root directory to work in
        string root = "/apps/myapp";

        /*
         * Set and retrieve a boolean key, see API for more information on how
         * to retrieve other types
         */

        // Sub directories are created automatically
        string key = root + "/foo/baz";
        gc.set_bool (key, true);

        bool result = gc.get_bool (key);
        stdout.printf ("Value of %s is %s\n", key, result.to_string ());

        /*
         * Getting a list of keys in a directory
         */

        // first, create a few more entries
        gc.set_bool (root + "/bar", true);
        gc.set_bool (root + "/baz", true);
        gc.set_bool (root + "/qux", true);

        SList<GConf.Entry> entries = gc.all_entries (root);
        stdout.printf ("Entry list has %u items\n", entries.length ());
        foreach (var entry in entries) {
            stdout.printf ("Key: %s Value: %s\n", entry.get_key (), 
                           entry.get_value ().get_bool ().to_string ());
        }

        /*
         * Getting a list of subdirectories in a directory
         */

        // first, create a few more directories
        gc.set_bool (root + "/bar/baz", true);
        gc.set_bool (root + "/baz/baz", true);
        gc.set_bool (root + "/qux/baz", true);

        SList<string> dir_list = gc.all_dirs (root);
        stdout.printf ("Directory list has %u items\n", dir_list.length ());
        foreach (string dir in dir_list) {
            stdout.printf ("Directory: %s\n", dir);
        }

        /*
         * Installing a callback function which gets called everytime
         * a certain key changes
         */

        string listening_dir = root + "/foo";
        gc.add_dir (listening_dir, GConf.ClientPreloadType.ONELEVEL);
        stdout.printf ("Listening on %s\n", listening_dir);

        string listening_key = listening_dir + "/baz";
        gc.notify_add (listening_key, gconf_cb);
        stdout.printf ("Having ourselves notified on %s\n", listening_key);

        /*
         * You may now run 'gconf-editor' and change the value for
         * key '/apps/myapp/foo/baz' or comment loop.run () to remove the key.
         */

        var loop = new MainLoop (null, false);
        loop.run ();

        // This operation unsets values from keys. If a key is unset or a
        // directory doesn't have subkeys anymore, they get deleted. They might
        // be visible with gconf-editor until GConf has collected its garbage
        // though. There is just one valid flag in GConf.UnsetFlags according to
        // http://references.valadoc.org/gconf-2.0/GConf.UnsetFlags.html
        gc.recursive_unset (root, GConf.UnsetFlags.NAMES);
    }

    /*
     * A callback which gets called everytime a defined key changes
     *
     * For API documentation on Entry and Value see:
     * http://references.valadoc.org/gconf-2.0/GConf.Entry.html
     * http://references.valadoc.org/gconf-2.0/GConf.Value.html
     */
    private static void gconf_cb (GConf.Client gc, uint cxnid, GConf.Entry entry) {
        // We silently assume, that this Value contains a bool, we should
        // check it first
        bool new_value = entry.get_value ().get_bool ();
        stdout.printf ("Callback called with entry %s, new value: %s\n",
                       entry.get_key (), new_value.to_string ());
    }
}

int main () {

    var sample = new GConfSample ();

    try {
        sample.run ();
    } catch (GLib.Error e) {
        stderr.printf ("%s\n", e.message);
    }

    return 0;
}
