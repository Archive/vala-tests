
// http://live.gnome.org/Vala/LibSoupSample vala-test:examples/soup-twitter.vala

using Soup;

void main () {
    // add your twitter username
    string username = "";

    // format the URL to use the username as the filename
    string url = "http://twitter.com/users/%s.xml".printf (username);

    stdout.printf ("Getting status for %s\n".printf (username));

    // create an HTTP session to twitter
    var session = new Soup.SessionAsync ();
    var message = new Soup.Message ("GET", url);

    // send the HTTP request
    session.send_message (message);

    // output the XML result to stdout 
    stdout.printf (message.response_body.flatten ().data);
}
