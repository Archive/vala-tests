
// http://live.gnome.org/Vala/LoudmouthSample vala-test:examples/lm-send-sync.vala

/*
 * Copyright (C) 2004 Imendio AB
 * Copyright (C) 2009 Harley Laue <losinggeneration@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Description:
 * A little program that let you send jabber messages to another person.
 * Note: The synchronous API is being removed in the 2.0 branch of Loudmouth.
 *
 * Build instructions:
 * valac --pkg loudmouth-1.0 lm-send-sync.vala
 */

using Lm;

class LmSyncDemo {

    static string server = null;
    static string message = null;
    static string username = null;
    static string password = null;
    static string recipient = null;
    static string resource;
    static uint port;

    const OptionEntry[] options = {
            { "server", 's', 0, OptionArg.STRING, ref server, "Server to connect to. You need to have a valid login on that server.", "server.org" },
            { "username", 'u', 0, OptionArg.STRING, ref username, "Username to used for the server you selected.", "some_username" },
            { "password", 'p', 0, OptionArg.STRING, ref password, "Password to use for entered username.", "some_password" },
            { "recipient",'t', 0, OptionArg.STRING, ref recipient, "User to send message to.", "someone@server.org" },
            { "message", 'm', 0, OptionArg.STRING, ref message, "Message to send to recipient.", "\"some message to send\"" },
            { "resource", 'r', OptionFlags.OPTIONAL_ARG, OptionArg.STRING, ref resource, "Resource to use when connecting.", "jabber-send" },
            { "port", 'o', OptionFlags.OPTIONAL_ARG, OptionArg.INT, ref port, "Port to use when connecting to selected server.", "5222" },
            { null }
        };

    static int main (string[] args) {
        resource = "jabber-send";
        port = Connection.DEFAULT_PORT;

        try {
            var opt_context = new OptionContext ("- Loudmouth Synchronous Sample");
            opt_context.set_help_enabled (true);
            opt_context.add_main_entries (options, null);
            opt_context.parse (ref args);
            if (server == null || message == null || recipient == null
                               || username == null || password == null)
            {
                print ("You must provide at least username, password, server, recipient, and message\n");
                print ("%s", opt_context.get_help (true, null));
                return 1;
            }
        } catch (OptionError e) {
            stdout.printf ("%s\n", e.message);
            stdout.printf ("Run '%s --help' to see a full list of available command line options.\n", args[0]);
            return 1;
        }

        var connection = new Connection (server);

        try {
            print ("Connecting to %s\n", server);
            connection.open_and_block ();

            print ("Authenticating as '%s' with '%s' and the resource '%s'\n",
                   username, password, resource);
            connection.authenticate_and_block (username, password, resource);

            var m = new Message (recipient, MessageType.MESSAGE);
            m.node.add_child ("body", message);

            print ("Sending message '%s' to %s\n", message, recipient);
            connection.send (m);

            print ("Closing connection\n");
            connection.close ();
        } catch (GLib.Error e) {
            stderr.printf ("Error: %s\n", e.message);
            return 1;
        } finally {
            /* This will become a lot easier with RAII support in
               Loudmouth 1.5.x. You won't need to manually close the connection
               and the whole 'finally' block will become unnecessary, since
               the connection will get closed by its destructor if it's open. */
            if (connection.is_open ()) {
                finally_close (connection);
            }
        }

        return 0;
    }

    static void finally_close (Connection connection) {
        try {
            connection.close ();
        } catch (GLib.Error e) {
            error ("Can't close connection.");
        }
    }
}
