
// http://live.gnome.org/Vala/GIOSamples vala-test:examples/gio-async-reading.vala

// Async stream reading with GIO in Vala sample code for async syntax 
// This example will need recent version of vala from git (after 14. September 2009)

public class AsyncReadLineTest: Object {
	public static GLib.MainLoop main_loop;
	private DataInputStream di_stream;
	private string adress;
	private string sourcetext = "";

	private signal void reading_finished();

	public AsyncReadLineTest(string adress = "http://www.gnome.org") {
		this.adress = adress;
		File f = File.new_for_uri(adress);
		try {
			di_stream = new DataInputStream(f.read(null));
		} catch (GLib.Error e) {
			print("Error 01!\n");
		}
		reading_finished.connect(on_reading_finished);
	}

	private async void read_something_async() {
		print("start...\n\n");
		size_t a;
		string buf = null;
		
		try {
			do {
				buf = yield di_stream.read_line_async(GLib.Priority.DEFAULT, null, out a);
				sourcetext = sourcetext + buf;
			} while (buf!=null);
		}
		catch(GLib.Error e) {
			print("%s\n", e.message);
		}
		print("\nREADY\n");
		reading_finished(); // signal finish
		return;
	}

	private void on_reading_finished() {
		print("sourcetext: \n%s\n", sourcetext);
		main_loop.quit();
	}

	public static int main(string[] args) {
		main_loop = new MainLoop(null, false);
		
		AsyncReadLineTest async_test;
		if(args[1]!=null) {
			File f = File.new_for_commandline_arg(args[1]);
		
			if(f.query_exists(null)) 
				async_test = new AsyncReadLineTest( f.get_uri());
			else
				async_test = new AsyncReadLineTest();
		}
		else {
			async_test = new AsyncReadLineTest();
		}
		async_test.read_something_async();
		main_loop.run();
		return 0;
	}
}
