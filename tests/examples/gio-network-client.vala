
// http://live.gnome.org/Vala/GIONetworkingSample vala-test:examples/gio-network-client.vala

void main () {
    try {
        // Resolve hostname to IP address
        var resolver = Resolver.get_default ();
        var addresses = resolver.lookup_by_name ("www.google.com", null);
        var address = addresses.nth_data (0);
        print ("(sync)  resolved www.google.com to %s\n", address.to_string ());

        // Connect
        var client = new SocketClient ();
        var conn = client.connect (new InetSocketAddress (address, 80), null);
        print ("(sync)  connected to www.google.com\n");

        // Send HTTP GET request
        string message = "GET / HTTP/1.1\r\nHost: www.google.com\r\n\r\n";
        conn.output_stream.write (message, message.size (), null);
        print ("(sync)  wrote request\n");

        // Receive response
        var input = new DataInputStream (conn.input_stream);
        message = input.read_line (null, null).strip ();
        print ("(sync)  received status line: %s\n", message);

    } catch (Error e) {
        stderr.printf ("%s\n", e.message);
    }
}
