
// http://live.gnome.org/Vala/GeeSamples vala-test:examples/gee-map.vala

using Gee;

void main () {

    var map = new HashMap<string, int> ();

    // Setting values
    map.set ("one", 1);
    map.set ("two", 2);
    map.set ("three", 3);
    map["four"] = 4;            // same as map.set ("four", 4)
    map["five"] = 5;

    // Getting values
    int a = map.get ("four");
    int b = map["four"];        // same as map.get ("four")
    assert (a == b);

    // Iteration

    stdout.printf ("Iterating over entries\n");
    foreach (var entry in map.entries) {
        stdout.printf ("%s => %d\n", entry.key, entry.value);
    }

    stdout.printf ("Iterating over keys only\n");
    foreach (string key in map.keys) {
        stdout.printf ("%s\n", key);
    }

    stdout.printf ("Iterating over values only\n");
    foreach (int value in map.values) {
        stdout.printf ("%d\n", value);
    }

    stdout.printf ("Iterating via 'while' statement\n");
    var iterator = map.map_iterator ();
    var has_next = iterator.first ();
    while (has_next == true) {
        stdout.printf ("%d\n", iterator.get_value ());
        has_next = iterator.next ();
    }
}
