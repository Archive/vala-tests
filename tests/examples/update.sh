#!/bin/sh

interactive="
async-gio
bluez-dbus-sample
cairo
dbus-sample
dbus-server-sample
egg-clock
gconf
glade
gnio-sock
gstreamer-square-beep
gstreamer-video
gtk-hello
gtk-builder-sample
gtk-filechooser
gtk-search-dialog
gtk-valawidget
panel-applet-advanced
panel-applet-simple
poppler-sample
webkit-sample
number-guessing
"

deprecated="
glade
"

set -e

while true ; do
    case "$1" in
	--check) check=1 ; shift ;;
	--interact) interact="--interact" ; shift ;;
	--) shift ; break ;;
	*) break ;;
    esac
done

for file in *.vala ; do

test=`basename $file .vala`

# hackish
PACKAGES=
grep "Gtk\." $file >/dev/null && PACKAGES="$PACKAGES --pkg gtk+-2.0"
grep -i " gio" $file >/dev/null && PACKAGES="$PACKAGES --pkg gio-2.0"
grep "DBus\." $file >/dev/null && PACKAGES="$PACKAGES --pkg dbus-glib-1"
grep -i "cairo" $file >/dev/null && PACKAGES="$PACKAGES --pkg cairo"
grep "GConf\." $file >/dev/null && PACKAGES="$PACKAGES --pkg gconf-2.0"
grep "Glade\." $file >/dev/null && PACKAGES="$PACKAGES --pkg libglade-2.0"
grep "Module\." $file >/dev/null && PACKAGES="$PACKAGES --pkg gmodule-2.0"
grep "Gnome\.Desktop" $file >/dev/null && PACKAGES="$PACKAGES --pkg gnome-desktop-2.0"
grep "GMenu\." $file >/dev/null && PACKAGES="$PACKAGES --pkg libgnome-menu"
grep "Gst\." $file >/dev/null && PACKAGES="$PACKAGES --pkg gstreamer-0.10"
grep "Gdk\.x11" $file >/dev/null && PACKAGES="$PACKAGES --pkg gdk-x11-2.0"
grep "XOverlay" $file >/dev/null && PACKAGES="$PACKAGES --pkg gstreamer-interfaces-0.10"
grep "using Panel" $file >/dev/null && PACKAGES="$PACKAGES --pkg libpanelapplet-2.0"
grep -i "pango" $file >/dev/null && PACKAGES="$PACKAGES --pkg pangocairo"
grep "Poppler\." $file >/dev/null && PACKAGES="$PACKAGES --pkg poppler-glib"
grep "SocketConnection" $file >/dev/null && PACKAGES="$PACKAGES --pkg gnio"
grep "WebKit" $file >/dev/null && PACKAGES="$PACKAGES --pkg webkit-1.0"
grep "Gee" $file >/dev/null && PACKAGES="$PACKAGES --pkg gee-1.0"

VALAFLAGS=
[ "$test" = "glade" ] && VALAFLAGS='-X "-Wl,--export-dynamic"'
[ "$test" = "gnome-desktop-and-menu" ] && VALAFLAGS='-X "-DGMENU_I_KNOW_THIS_IS_UNSTABLE"'
[ "$test" = "preprocessor" ] && VALAFLAGS='-D FOO -D BAR -D FOOBAR -D NOFOO'

TESTARGS=
[ "$test" = "pango-cairo" ] && TESTARGS="../Makefile"

RUN=1
for nr in $interactive ; do
  [ $nr = $test ] && RUN=0
done

cat >$test.test <<EOF
#!/bin/sh

set -e

SRCDIR=../tests/examples

if [ "x\$VALAC" = "x" ] ; then
  VALAC=valac
  SRCDIR=\`dirname \$0\`
fi

while true ; do
    case "\$1" in
	--interact) INTERACT=1 ; shift ;;
	--) shift ; break ;;
	*) break ;;
    esac
done

TESTNAME=\`basename \$0 .test\`

\$VALAC $PACKAGES -o \$TESTNAME \$SRCDIR/\$TESTNAME.vala $VALAFLAGS \$VALAFLAGS

if [ "x$RUN" = "x1" -o "x\$INTERACT" = "x1" ] ; then
  ./\$TESTNAME $TESTARGS \$@
else
  echo ""
  echo "*** WARNING: This vala test is interactive and will not be run (try --interact) ***"
fi

EOF

chmod +x $test.test

[ "$check" = "1" ] && echo "Running $test..." && ./$test.test $interact $@

rm -f $test

done
