
// http://live.gnome.org/Vala/TimeSample vala-test:examples/time.vala

public class Main : GLib.Object {

    static int main (string[] args) {
        GLib.Time myTime = GLib.Time();
        string input_string = "2009-03-03T10:53:47+00:00";
        myTime.strptime ( input_string, "%Y-%m-%dT%T");
        stdout.printf("Original date: %s, Parsed date: %s\n",input_string, myTime.format("%FT%T%z"));
        return 0;
    }
}
