
// http://live.gnome.org/Vala/LibSoupSample vala-test:examples/soup-xmlrpc-test-hello.vala

using Soup;

void main () {
    var message = xmlrpc_request_new ("http://kushaldas.wordpress.com/xmlrpc.php",
                                     "demo.sayHello");
    var session = new SessionSync ();
    session.send_message (message);

    string data = message.response_body.flatten ().data;
    try {
        Value v = Value (typeof (string));
        xmlrpc_parse_method_response (data, -1, v);
        stdout.printf ("Got: %s\n", (string) v);
    } catch (Error e) {
        stderr.printf ("Error while processing the response: %s\n", e.message);
    }
}
