
// http://live.gnome.org/Vala/InputSamples vala-test:examples/input-stdin-line.vala

int main () {
    stdout.printf ("Please enter your name: ");
    string? name = stdin.read_line ();
    if (name != null) {
        stdout.printf ("Hello, %s!\n", name);
    }
    return 0;
}
