
// http://live.gnome.org/Vala/CouchDBSample vala-test:examples/couchdb.vala

const string DB1 = "vala1";
const string DB2 = "vala2";

int main () {

    /* Connect to CouchDB */
    var conn = new CouchDB.Session ();
//  var conn = new CouchDB.Session ("http://localhost:5984");
    stdout.printf ("Connecting to CouchDB at %s\n", conn.get_uri ());

    try {

        /* Create demo databases */
        stdout.printf ("Creating databases\n");
        conn.create_database (DB1);
        conn.create_database (DB2);

        /* Create a document */
        var newdoc = new CouchDB.Document (conn);
        newdoc.set_boolean_field ("awesome", true);
        newdoc.set_string_field ("phone", "555-VALA");
        newdoc.set_double_field ("pi", 3.14159);
        newdoc.set_int_field ("meaning_of_life", 42);
        newdoc.put (DB1);    // store document

        /* ... another one, this time with a custom ID and a StructField */
        newdoc = new CouchDB.Document (conn);
        newdoc.set_id ("my_id");    // custom ID, otherwise auto-generated
        newdoc.set_string_field ("color", "green");
        var sf = new CouchDB.StructField ();
        sf.set_int_field ("foo", 47);
        sf.set_int_field ("bar", 11);
        newdoc.set_struct_field ("mystruct", sf);
        newdoc.put (DB1);

        /* Query document by ID */
        var qdoc = conn.get_document (DB1, "my_id");
        assert (qdoc.get_string_field ("color") == "green");
        assert (qdoc.get_struct_field ("mystruct").has_field ("foo"));
        assert (qdoc.get_struct_field ("mystruct").get_int_field ("foo") == 47);

        /* List database informations and contents */
        var dbases = conn.list_databases ();
        foreach (var dbase in dbases) {
            stdout.printf ("database: %s\n", dbase);
            var dbinfo = conn.get_database_info (dbase);
            stdout.printf ("* dbname: %s\n", dbinfo.get_dbname ());
            stdout.printf ("* documents_count: %d\n", dbinfo.get_documents_count ());
            stdout.printf ("* deleted_documents_count: %d\n", dbinfo.get_deleted_documents_count ());
            stdout.printf ("* update_sequence: %d\n", dbinfo.get_update_sequence ());
            stdout.printf ("* compact_running: %s\n", dbinfo.is_compact_running ().to_string ());
            stdout.printf ("* disk_size: %d\n", dbinfo.get_disk_size ());
            foreach (var docinfo in conn.list_documents (dbase)) {
                stdout.printf ("* document docid: %s revision: %s\n", docinfo.get_docid (),
                                                                      docinfo.get_revision ());
                var doc = conn.get_document (dbase, docinfo.get_docid ());
                stdout.printf ("** full content: %s\n", doc.to_string ());
            }
        }

        /* Delete a document */
        qdoc.delete ();

        /* Delete demo databases */
        stdout.printf ("Deleting databases\n");
        conn.delete_database (DB1);
        conn.delete_database (DB2);

    } catch (Error e) {
        stderr.printf ("%s\n", e.message);
    }

    return 0;
}
