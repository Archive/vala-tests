
// http://live.gnome.org/Vala/XmlSample vala-test:examples/libxml2.vala

/*
 * Various operations with libxml2: Parsing and creating an XML file
 */

using Xml;

class XmlSample {

    // Line indentation
    private int indent = 0;

    private void print_indent (string node, string content, char token = '+') {
        string indent = string.nfill (this.indent * 2, ' ');
        stdout.printf ("%s%c%s: %s\n", indent, token, node, content);
    }

    public void parse_file (string path) {
        // Parse the document from path
        Xml.Doc* doc = Parser.parse_file (path);
        if (doc == null) {
            stderr.printf ("File %s not found or permissions missing", path);
            return;
        }

        // Get the root node. notice the dereferencing operator -> instead of .
        Xml.Node* root = doc->get_root_element ();
        if (root == null) {
            // Free the document manually before returning
            delete doc;
            stderr.printf ("The xml file '%s' is empty", path);
            return;
        }

        print_indent ("XML document", path, '-');

        // Print the root node's name
        print_indent ("Root node", root->name);

        // Let's parse those nodes
        parse_node (root);

        // Free the document
        delete doc;
    }

    private void parse_node (Xml.Node* node) {
        this.indent++;
        // Loop over the passed node's children
        for (Xml.Node* iter = node->children; iter != null; iter = iter->next) {
            // Spaces between tags are also nodes, discard them
            if (iter->type != ElementType.ELEMENT_NODE) {
                continue;
            }

            // Get the node's name
            string node_name = iter->name;
            // Get the node's content with <tags> stripped
            string node_content = iter->get_content ();
            print_indent (node_name, node_content);

            // Now parse the node's properties (attributes) ...
            parse_properties (iter);

            // Followed by its children nodes
            parse_node (iter);
        }
        this.indent--;
    }

    private void parse_properties (Xml.Node* node) {
        // Loop over the passed node's properties (attributes)
        for (Xml.Attr* prop = node->properties; prop != null; prop = prop->next) {
            string attr_name = prop->name;

            // Notice the ->children which points to a Node*
            // (Attr doesn't feature content)
            string attr_content = prop->children->content;
            print_indent (attr_name, attr_content, '|');
        }
    }

    public static string create_simple_xml () {
        Xml.Doc* doc = new Xml.Doc ("1.0");

        Xml.Ns* ns = new Xml.Ns (null, "", "foo");
        ns->type = Xml.ElementType.ELEMENT_NODE;
        Xml.Node* root = new Xml.Node (ns, "simple");
        doc->set_root_element (root);

        root->new_prop ("property", "value");

        Xml.Node* subnode = root->new_text_child (ns, "subnode", "");
        subnode->new_text_child (ns, "textchild", "another text" );
        subnode->new_prop ("subprop", "escaping \" and  < and >" );

        Xml.Node* comment = new Xml.Node.comment ("This is a comment");
        root->add_child (comment);

        string xmlstr;
        // This throws a compiler warning, see bug 547364
        doc->dump_memory (out xmlstr);

        delete doc;
        return xmlstr;
    }
}

int main (string[] args) {

    if (args.length < 2) {
        stderr.printf ("Argument required!\n");
        return 1;
    }

    // Initialisation, not instantiation since the parser is a static class
    Parser.init ();

    string simple_xml = XmlSample.create_simple_xml ();
    stdout.printf ("Simple XML is:\n%s\n", simple_xml);

    var sample = new XmlSample ();
    // Parse the file listed in the first passed argument
    sample.parse_file (args[1]);

    // Do the parser cleanup to free the used memory
    Parser.cleanup ();

    return 0;
}
