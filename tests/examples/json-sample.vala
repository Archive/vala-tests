
// http://live.gnome.org/Vala/JsonSample vala-test:examples/json-sample.vala

void main () {
    var uri = "http://ws.geonames.org/searchJSON?formatted=true&q=%s&maxRows=100&lang=en&style=medium".printf ("asakusa");

    var session = new Soup.SessionSync ();
    var message = new Soup.Message ("GET", uri);
    session.send_message (message);

    try {
        var parser = new Json.Parser ();
        parser.load_from_data (message.response_body.flatten ().data, -1);

        var root_object = parser.get_root ().get_object ();
        int64 count = root_object.get_int_member ("totalResultsCount");
        stdout.printf ("%lld results:\n\n", count);

        foreach (var geonode in root_object.get_array_member ("geonames").get_elements ()) {
            var geoname = geonode.get_object ();
            stdout.printf ("%s\n%s\n%f\n%f\n\n",
                          geoname.get_string_member ("name"),
                          geoname.get_string_member ("countryName"),
                          geoname.get_double_member ("lng"),
                          geoname.get_double_member ("lat"));
        }
    } catch (Error e) {
        stderr.printf ("I guess something is not working...\n");
    }
}
