
// http://live.gnome.org/Vala/DBusServerSample vala-test:examples/gdbus-demo-client.vala

[DBus (name = "org.example.Demo")]
interface Demo : Object {
    public abstract int ping (string msg) throws IOError;
}

void main () {
    try {
        Demo demo = Bus.get_proxy_sync (BusType.SESSION, "org.example.Demo",
                                                         "/org/example/demo");

        int pong = demo.ping ("Hello from Vala");
        stdout.printf ("%d\n", pong);

    } catch (IOError e) {
        stderr.printf ("%s\n", e.message);
    }
}
