
// http://live.gnome.org/Vala/CairoSample vala-test:examples/cairo-threaded.vala

using Gtk;
using Gdk;

public class CairoThreadedExample : Gtk.Window {

    // the global pixmap that will serve as our buffer
    private Gdk.Pixmap pixmap;

    private int oldw;
    private int oldh;

    private int currently_drawing;
    private int i_draw;

    private unowned Thread thread_info;
    private bool first_execution = true;

    public CairoThreadedExample () {
        // constructor chain up
        GLib.Object (type: Gtk.WindowType.TOPLEVEL);
        // set_window size
        set_size_request (500, 500);

        // this must be done before we define our pixmap so that it can
        // reference the colour depth and such
        show_all ();

        // set up our pixmap so it is ready for drawing
        this.pixmap = new Gdk.Pixmap (this.window, 500, 500, -1);

        // because we will be painting our pixmap manually during expose events
        // we can turn off gtk's automatic painting and double buffering routines.
        this.app_paintable = true;
        this.double_buffered = false;

        // Signals
        this.destroy.connect (Gtk.main_quit);
        this.expose_event.connect (on_window_expose_event);
        this.configure_event.connect (on_window_configure_event);
    }

    public void run () {
        // Timeout repeatedly calls closure every 100 ms after it returned true
        Timeout.add (100, timer_exe);
    }

    private bool on_window_configure_event (Gtk.Widget sender, Gdk.EventConfigure event) {
        // make our selves a properly sized pixmap if our window has been resized
        if (oldw != event.width || oldh != event.height) {
            // create our new pixmap with the correct size.
            var tmppixmap = new Gdk.Pixmap (this.window, event.width, event.height, -1);
            // copy the contents of the old pixmap to the new pixmap.
            // This keeps ugly uninitialized pixmaps from being painted upon
            // resize
            int minw = oldw, minh = oldh;
            if (event.width < minw) {
                minw = event.width;
            }
            if (event.height < minh) {
                minh = event.height;
            }
            Gdk.draw_drawable (tmppixmap, this.style.fg_gc[this.get_state ()],
                               pixmap, 0, 0, 0, 0, minw, minh);
            // we're done with our old pixmap, so we can get rid of it and
            // replace it with our properly-sized one.
            pixmap = tmppixmap;
        }
        oldw = event.width;
        oldh = event.height;
        return true;
    }

    private bool on_window_expose_event (Gtk.Widget da, Gdk.EventExpose event) {
        da.window.draw_drawable (this.style.fg_gc[this.get_state ()], pixmap,
                                 // Only copy the area that was exposed.
                                 event.area.x, event.area.y,
                                 event.area.x, event.area.y,
                                 event.area.width, event.area.height);
        return true;
    }

    // do_draw will be executed in a separate thread whenever we would like to
    // update our animation
    private void* do_draw () {
        int width, height;
        currently_drawing = 1;

        Gdk.threads_enter ();
        pixmap.get_size (out width, out height);
        Gdk.threads_leave ();

        //create a gtk-independant surface to draw on
        var cst = new Cairo.ImageSurface (Cairo.Format.ARGB32, width, height);
        var cr = new Cairo.Context (cst);

        // do some time-consuming drawing
        i_draw++;
        i_draw = i_draw % 300;   // give a little movement to our animation
        cr.set_source_rgb (0.9, 0.9, 0.9);
        cr.paint ();
        // let's just redraw lots of times to use a lot of proc power
        for (int k = 0; k < 100; k++) {
            for (int j = 0; j < 1000; j++) {
                cr.set_source_rgb ((double) j / 1000.0, (double) j / 1000.0,
                             1.0 - (double) j / 1000.0);
                cr.move_to (i_draw, j / 2); 
                cr.line_to (i_draw + 100, j / 2);
                cr.stroke ();
            }
        }

        // When dealing with gdkPixmap's, we need to make sure not to
        // access them from outside Gtk.main().
        Gdk.threads_enter ();

        var cr_pixmap = Gdk.cairo_create (pixmap);
        cr_pixmap.set_source_surface (cst, 0, 0);
        cr_pixmap.paint ();

        Gdk.threads_leave ();

        currently_drawing = 0;
        return null;
    }

    private bool timer_exe () {
        // use a safe function to get the value of currently_drawing so
        // we don't run into the usual multithreading issues
        int drawing_status = AtomicInt.get (ref currently_drawing);

        // if we are not currently drawing anything, launch a thread to 
        // update our pixmap
        if (drawing_status == 0) {
            if (first_execution != true) {
                thread_info.join ();
            }
            try {
                thread_info = Thread.create (do_draw, true);
            } catch (Error e) {
                stderr.printf ("%s\n", e.message);
            }
        }

        // tell our window it is time to draw our animation.
        int width, height;
        pixmap.get_size (out width, out height);
        queue_draw_area (0, 0, width, height);
        first_execution = false;
        return true;
    }

    static int main (string[] args){
        Gdk.threads_init ();
        Gdk.threads_enter ();

        Gtk.init (ref args);
        var window = new CairoThreadedExample ();
        window.run ();
        Gtk.main ();

        Gdk.threads_leave ();

        return 0;
    }
}
