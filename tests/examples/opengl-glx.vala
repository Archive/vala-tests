
// http://live.gnome.org/Vala/OpenGLSamples vala-test:examples/opengl-glx.vala

using Gtk;
using Gdk;
using GLX;
using GL;

class GLXSample : Gtk.Window {

    private X.Display xdisplay;
    private GLX.Context context;
    private XVisualInfo xvinfo;

    public GLXSample () {
        this.title = "OpenGL with GLX";
        set_reallocate_redraws (true);
        destroy.connect (Gtk.main_quit);

        int[] attrlist = {
            GLX_RGBA,
            GLX_RED_SIZE, 1,
            GLX_GREEN_SIZE, 1,
            GLX_BLUE_SIZE, 1,
            GLX_DOUBLEBUFFER, 0
        };

        this.xdisplay = x11_get_default_xdisplay ();
        if (!glXQueryExtension (xdisplay, null, null)) {
            stderr.printf ("OpenGL not supported\n");
        }

        this.xvinfo = glXChooseVisual (xdisplay, x11_get_default_screen (), attrlist);
        if (xvinfo == null) {
            stderr.printf ("Error configuring OpenGL\n");
        }

        var drawing_area = new DrawingArea ();
        drawing_area.set_size_request (300, 300);
        drawing_area.set_double_buffered (false);

        this.context = glXCreateContext (xdisplay, xvinfo, null, true);

        drawing_area.configure_event.connect (on_configure_event);
        drawing_area.expose_event.connect (on_expose_event);

        add (drawing_area);
    }

    private bool on_configure_event (Widget widget, Gdk.EventConfigure event) {
        if (!glXMakeCurrent (xdisplay, x11_drawable_get_xid (widget.window), context))
            return false;

        glViewport (0, 0, (GLsizei) widget.allocation.width,
                          (GLsizei) widget.allocation.height);

        return true;
    }

    private bool on_expose_event (Widget widget, Gdk.EventExpose event) {
        if (!glXMakeCurrent (xdisplay, x11_drawable_get_xid (widget.window), context))
            return false;

        glClear (GL_COLOR_BUFFER_BIT);

        glBegin (GL_TRIANGLES);
            glIndexi (0);
            glColor3f (1.0f, 0.0f, 0.0f);
            glVertex2i (0, 1);
            glIndexi (0);
            glColor3f (0.0f, 1.0f, 0.0f);
            glVertex2i (-1, -1);
            glIndexi (0);
            glColor3f (0.0f, 0.0f, 1.0f);
            glVertex2i (1, -1);
        glEnd ();

        glXSwapBuffers (xdisplay, x11_drawable_get_xid (widget.window));

        return true;
    }
}

void main (string[] args) {
    Gtk.init (ref args);

    var sample = new GLXSample ();
    sample.show_all ();

    Gtk.main ();
}
