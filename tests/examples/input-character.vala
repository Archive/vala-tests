
// http://live.gnome.org/Vala/InputSamples vala-test:examples/input-character.vala

public static void main (string[] args) {
    int c = 0;
    stdout.printf ("Type something and press enter. Type '0' to quit\n");
    do {
        c = stdin.getc ();
        if (c > 10) {
            stdout.printf ("%c (%d)\n", c, c);
        }
    } while (c != '0');
}
