
// http://live.gnome.org/Vala/DBusServerSample vala-test:examples/dbus-demo-client.vala

[DBus (name = "org.example.Demo")]
interface Demo : Object {
    public abstract int ping (string msg) throws DBus.Error;
}

void main () {
    try {
        var conn = DBus.Bus.get (DBus.BusType.SESSION);
        var demo = (Demo) conn.get_object ("org.example.Demo",
                                           "/org/example/demo");

        int pong = demo.ping ("Hello from Vala");
        stdout.printf ("%d\n", pong);

    } catch (DBus.Error e) {
        stderr.printf ("%s\n", e.message);
    }
}
