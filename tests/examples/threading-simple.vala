
// http://live.gnome.org/Vala/ThreadingSamples vala-test:examples/threading-simple.vala

class MyThread {

    private string name;
    private int count = 0;

    public MyThread (string name) {
        this.name = name;
    }

    public void* thread_func () {
        while (true) {
            stdout.printf ("%s: %i\n", this.name, this.count);
            this.count++;
        }
    }
}

static int main (string[] args) {
    if (!Thread.supported ()) {
        error ("Cannot run without thread support.");
    }

    var thread_a_data = new MyThread ("A");
    var thread_b_data = new MyThread ("B");

    try {
        unowned Thread thread_a = Thread.create (thread_a_data.thread_func, true);
        unowned Thread thread_b = Thread.create (thread_b_data.thread_func, true);
        thread_a.join ();
        thread_b.join ();
    } catch (ThreadError e) {
        return 1;
    }

    return 0;
}
