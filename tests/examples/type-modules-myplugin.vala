
// http://live.gnome.org/Vala/TypeModules vala-test:examples/type-modules-myplugin.vala

class MyPlugin : Object, TestPlugin {
    public void hello () {
        stdout.printf ("Hello world!\n");
    }
}

[ModuleInit]
public Type register_plugin (TypeModule module) {
    // types are registered automatically
    return typeof (MyPlugin);
}
