
// http://live.gnome.org/Vala/PanelAppletSample vala-test:examples/panel-applet-advanced.vala

using Panel;

public class MainApplet : Panel.Applet {

    public static bool factory (Applet applet, string iid) {
        ((MainApplet) applet).create ();
        return true;
    }

    private void on_change_background (Panel.Applet applet, Panel.AppletBackgroundType type,
                                       Gdk.Color? color, Gdk.Pixmap? pixmap) {
        applet.set_style (null);

        var rc_style = new Gtk.RcStyle ();
        applet.modify_style (rc_style);

        switch (type) {
        case Panel.AppletBackgroundType.COLOR_BACKGROUND:
            applet.modify_bg (Gtk.StateType.NORMAL, color);
            break;
        case Panel.AppletBackgroundType.PIXMAP_BACKGROUND:
            applet.style.bg_pixmap[0] = pixmap;
            set_style (style);
            break;
        }
    }

    private void create () {
        change_background.connect (on_change_background);

        var label = new Gtk.Label ("Vala Panel");
        add (label);

        string menu_definition = 
            "<popup name=\"button3\">" +
                "<menuitem debuname=\"About\" verb=\"About\" _label=\"_About...\" pixtype=\"stock\" pixname=\"gnome-stock-about\"/>" +
            "</popup>";

        var verb = BonoboUI.Verb ();
        verb.cname = "About";
        verb.cb = on_about_clicked;

        var verbs = new BonoboUI.Verb[] { verb };
        setup_menu (menu_definition, verbs, null);

        show_all();
    }
        
    private static void on_about_clicked (BonoboUI.Component component,
                                          void* user_data, string cname) {
        var dialog = new Gtk.MessageDialog (
            null,
            Gtk.DialogFlags.DESTROY_WITH_PARENT,
            Gtk.MessageType.ERROR,
            Gtk.ButtonsType.CLOSE,
            "About");
        dialog.secondary_text = "About dialog";
        dialog.run ();
        dialog.destroy ();
    }

    public static int main (string[] args) {
        var program = Gnome.Program.init ("Vala_Applet", "0", Gnome.libgnomeui_module,
                                          args, "sm-connect", false);
        return Applet.factory_main ("OAFIID:Vala_Applet_Factory",
                                    typeof (MainApplet), factory);
    }
}
