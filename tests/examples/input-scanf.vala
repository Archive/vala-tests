
// http://live.gnome.org/Vala/InputSamples vala-test:examples/input-scanf.vala

public static void main() {

    float f;
    double d;
    int i;
    long l;

    stdout.printf("Enter a float   : ");
    stdin.scanf("%f", out f);

    stdout.printf("Enter a double  : ");
    stdin.scanf("%lf", out d);

    stdout.printf("Enter an integer: ");
    stdin.scanf("%d", out i);

    stdout.printf("Enter a long    : ");
    stdin.scanf("%ld", out l);

    stdout.printf("The numbers you entered\n");
    stdout.printf("Float  : %f\n",f);
    stdout.printf("Double : %lf\n",d);
    stdout.printf("Integer: %d\n",i);
    stdout.printf("Long   : %ld\n",l);

}
