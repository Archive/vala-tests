
// http://live.gnome.org/Vala/HildonSample vala-test:examples/hildon-sample.vala

/* Hildon Vala Sample Code */
using Gtk;
using Hildon;

public class Sample : Hildon.Program {

    private Hildon.Window window;

    construct {
        window = new Hildon.Window ();
        window.destroy.connect (Gtk.main_quit);

        add_window (window);

        Environment.set_application_name ("Hildon Vala Sample");

        var label = new Gtk.Label ("Vala for Hildon Desktop!");
        var button = new Gtk.Button.with_label ("Press Me!");

        button.clicked.connect (() => {
            label.set_markup ("<big><b>Hello Vala!</b></big>");
        });

        var vbox = new Gtk.VBox (false, 2);

        vbox.pack_start (label, true, true, 2);
        vbox.pack_start (button, false, true, 2);

        window.add (vbox);
    }

    public void run () {
        window.show_all ();
        Gtk.main ();
    }

    static int main (string[] args) {
        Gtk.init (ref args);

        var app = new Sample ();
        app.run ();

        return 0;
    }
}
