
// http://live.gnome.org/Vala/OpenGLSamples vala-test:examples/opengl-gtkglext-spot.vala

using Gtk;
using Gdk;
using GL;
using GLU;

class SpotSample : Gtk.Window {

    static GLfloat xRot = 0.0f;
    static GLfloat yRot = 0.0f;

    static int iShade = 2;
    static int iTess = 3;

    static const GLfloat[] lightPos = { 0.0f, 0.0f, 75.0f, 1.0f };
    static const GLfloat[] specular = { 1.0f, 1.0f, 1.0f, 1.0f };
    static const GLfloat[] specref =  { 1.0f, 1.0f, 1.0f, 1.0f };
    static const GLfloat[] ambientLight = { 0.5f, 0.5f, 0.5f, 1.0f };
    static const GLfloat[] spotDir = { 0.0f, 0.0f, -1.0f };

    public SpotSample () {
        this.title = "OpenGL with GtkGLExt";
        this.destroy.connect (Gtk.main_quit);
        set_reallocate_redraws (true);

        var drawing_area = new DrawingArea ();
        drawing_area.set_size_request (800, 600);

        var glconfig = new GLConfig.by_mode (GLConfigMode.RGB
                                           | GLConfigMode.DOUBLE
                                           | GLConfigMode.DEPTH);

        WidgetGL.set_gl_capability (drawing_area, glconfig, null, true,
                                    GLRenderType.RGBA_TYPE);

        drawing_area.realize.connect (on_realize);
        drawing_area.configure_event.connect (on_configure_event);
        drawing_area.expose_event.connect (on_expose_event);

        drawing_area.add_events (Gdk.EventMask.KEY_PRESS_MASK);
        drawing_area.can_focus = true;
        drawing_area.key_press_event.connect (on_key_press_event);

        add (drawing_area);
    }

    /* Widget gets initialized */
    private void on_realize (Widget widget) {
        GLContext glcontext = WidgetGL.get_gl_context (widget);
        GLDrawable gldrawable = WidgetGL.get_gl_drawable (widget);

        if (!gldrawable.gl_begin (glcontext))
            return;

        glEnable (GL_DEPTH_TEST);
        glEnable (GL_CULL_FACE);
        glFrontFace (GL_CCW);

        glEnable (GL_LIGHTING);

        glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambientLight);

        glLightfv (GL_LIGHT0, GL_DIFFUSE, ambientLight);
        glLightfv (GL_LIGHT0, GL_SPECULAR, specular);
        glLightfv (GL_LIGHT0, GL_POSITION, lightPos);

        glLightf (GL_LIGHT0, GL_SPOT_CUTOFF, 50.0f);

        glEnable (GL_LIGHT0);

        glEnable (GL_COLOR_MATERIAL);

        glColorMaterial (GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

        glMaterialfv (GL_FRONT, GL_SPECULAR, specref);
        glMateriali (GL_FRONT, GL_SHININESS,128);

        glClearColor (0.0f, 0.0f, 0.0f, 1.0f);

        gldrawable.gl_end ();
    }

    /* Widget is resized */
    private bool on_configure_event (Widget widget, EventConfigure event) {
        GLContext glcontext = WidgetGL.get_gl_context (widget);
        GLDrawable gldrawable = WidgetGL.get_gl_drawable (widget);

        if (!gldrawable.gl_begin (glcontext))
            return false;

        int w = widget.allocation.width;
        int h = widget.allocation.height;

        glViewport (0, 0, (GLsizei) w, (GLsizei) h);

        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();

        GLfloat fAspect = (GLfloat) w / (GLfloat) h;
        gluPerspective (35.0f, fAspect, 1.0f, 500.0f);

        glMatrixMode (GL_MODELVIEW);
        glLoadIdentity ();
        glTranslatef (0.0f, 0.0f, -250.0f);

        gldrawable.gl_end ();
        return true;
    }

    /* Widget is asked to paint itself */
    private bool on_expose_event (Widget widget, EventExpose event) {
        GLContext glcontext = WidgetGL.get_gl_context (widget);
        GLDrawable gldrawable = WidgetGL.get_gl_drawable (widget);

        if (!gldrawable.gl_begin (glcontext))
            return false;

        if (iShade == 1)
            glShadeModel (GL_FLAT);
        else
            glShadeModel (GL_SMOOTH);

        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glPushMatrix ();
        glRotatef (xRot, 1.0f, 0.0f, 0.0f);
        glRotatef (yRot, 0.0f, 1.0f, 0.0f);

        glLightfv (GL_LIGHT0, GL_POSITION, lightPos);
        glLightfv (GL_LIGHT0, GL_SPOT_DIRECTION, spotDir);

        glColor3ub (255,0,0);

        glTranslatef (lightPos[0], lightPos[1], lightPos[2]);
        GLDraw.cone (true, 4.0f, 6.0f, 15, 15);

        glPushAttrib (GL_LIGHTING_BIT);

        glDisable (GL_LIGHTING);
        glColor3ub (255,255,0);
        GLDraw.sphere (true, 3.0f, 15, 15);

        glPopAttrib ();

        glPopMatrix ();

        glColor3ub (0, 0, 255);

        if (iTess == 1) {
            GLDraw.sphere (true, 30.0f, 7, 7);
        } else {
            if (iTess == 2)
                GLDraw.sphere (true, 30.0f, 15, 15);
            else
                GLDraw.sphere (true, 30.0f, 50, 50);
        }

        if (gldrawable.is_double_buffered ())
            gldrawable.swap_buffers ();
        else
            glFlush ();

        gldrawable.gl_end ();
        return true;
    }

    /* A key was pressed */
    private bool on_key_press_event (Widget drawing_area, EventKey event) {
        string key = Gdk.keyval_name (event.keyval);

        if (key == "Up")
            xRot-= 5.0f;

        if (key == "Down")
            xRot += 5.0f;

        if (key == "Left")
            yRot -= 5.0f;

        if (key == "Right")
            yRot += 5.0f;

        if (xRot > 356.0f)
            xRot = 0.0f;

        if (xRot < -1.0f)
            xRot = 355.0f;

        if (yRot > 356.0f)
            yRot = 0.0f;

        if (yRot < -1.0f)
            yRot = 355.0f;

        queue_draw ();
        return true;
    }
}

void main (string[] args) {
    Gtk.init (ref args);
    Gtk.gl_init (ref args);

    var sample = new SpotSample ();
    sample.show_all ();

    Gtk.main ();
}
