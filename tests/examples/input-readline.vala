
// http://live.gnome.org/Vala/InputSamples vala-test:examples/input-readline.vala

void main () {
    while (true) {
        var name = Readline.readline ("Please enter your name: ");
        if (name != null && name != "") {
            stdout.printf ("Hello, %s\n", name);
            Readline.History.add (name);
        }
    }
}
