
// http://live.gnome.org/Vala/InputSamples vala-test:examples/input-stdin.vala

/*
 * We use the fact that char[] allocates space
 * and that FileStream.gets takes a char[] but returns a string!
 *
 * Note that stdin.gets uses the "safe" fgets(), not the unsafe gets()
 */
string read_stdin () {
    var input = new StringBuilder ();
    var buffer = new char[1024];
    while (!stdin.eof ()) {
        string read_chunk = stdin.gets (buffer);
        if (read_chunk != null) {
            input.append (read_chunk);
        }
    }
    return input.str;
}

int main () {
    string name = read_stdin ();
    stdout.printf ("\n-----\n%s\n", name);
    return 0;
}
