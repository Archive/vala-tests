
// http://live.gnome.org/Vala/OpenGLSamples vala-test:examples/opengl-gtkglext.vala

using Gtk;
using Gdk;
using GL;

class GtkGLExtSample : Gtk.Window {

    public GtkGLExtSample () {
        this.title = "OpenGL with GtkGLExt";
        this.destroy.connect (Gtk.main_quit);
        set_reallocate_redraws (true);

        var drawing_area = new DrawingArea ();
        drawing_area.set_size_request (200, 200);

        var glconfig = new GLConfig.by_mode (GLConfigMode.RGB
                                           | GLConfigMode.DOUBLE);

        WidgetGL.set_gl_capability (drawing_area, glconfig, null, true,
                                    GLRenderType.RGBA_TYPE);

        drawing_area.configure_event.connect (on_configure_event);
        drawing_area.expose_event.connect (on_expose_event);

        add (drawing_area);
    }

    /* Widget is resized */
    private bool on_configure_event (Widget widget, EventConfigure event) {
        GLContext glcontext = WidgetGL.get_gl_context (widget);
        GLDrawable gldrawable = WidgetGL.get_gl_drawable (widget);

        if (!gldrawable.gl_begin (glcontext))
            return false;

        glViewport (0, 0, (GLsizei) widget.allocation.width,
                          (GLsizei) widget.allocation.height);

        gldrawable.gl_end ();
        return true;
    }

    /* Widget is asked to paint itself */
    private bool on_expose_event (Widget widget, EventExpose event) {
        GLContext glcontext = WidgetGL.get_gl_context (widget);
        GLDrawable gldrawable = WidgetGL.get_gl_drawable (widget);

        if (!gldrawable.gl_begin (glcontext))
            return false;

        glClear (GL_COLOR_BUFFER_BIT);

        glBegin (GL_TRIANGLES);
            glIndexi (0);
            glColor3f (1.0f, 0.0f, 0.0f);
            glVertex2i (0, 1);
            glIndexi (0);
            glColor3f (0.0f, 1.0f, 0.0f);
            glVertex2i (-1, -1);
            glIndexi (0);
            glColor3f (0.0f, 0.0f, 1.0f);
            glVertex2i (1, -1);
        glEnd ();

        if (gldrawable.is_double_buffered ())
            gldrawable.swap_buffers ();
        else
            glFlush ();

        gldrawable.gl_end ();
        return true;
    }
}

void main (string[] args) {
    Gtk.init (ref args);
    Gtk.gl_init (ref args);

    var sample = new GtkGLExtSample ();
    sample.show_all ();

    Gtk.main ();
}
