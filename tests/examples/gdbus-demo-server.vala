
// http://live.gnome.org/Vala/DBusServerSample vala-test:examples/gdbus-demo-server.vala

[DBus (name = "org.example.Demo")]
public class DemoServer : Object {
    private int counter;

    public int ping (string msg) {
        stdout.printf ("%s\n", msg);
        return counter++;
    }
}

void main () {
    try {
        var conn = Bus.get_sync (BusType.SESSION);
        conn.register_object ("/org/example/demo", new DemoServer ());

        var app = new Application ("org.example.Demo");
        app.run ();

    } catch (IOError e) {
        stderr.printf ("%s\n", e.message);
    }
}
