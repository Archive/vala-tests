using GLib;

private static const string test_a = "Item A" + " + Item B";
private static const string test_b = "Item C";
private static const string MY_DATA_PATH = "A Path";

struct MyStruct
{
	public string filename;
	public int id;
}

public class FunWithConstants : GLib.Object
{
	public const string test_c = "Item D" + " + " + "Item E";
	
	public string test = "String1" + "String2" + "String3";

	public string test_string_1 { public get; private set; default = "String"; }
	public string test_string_2 { public get; private set; default = "String1" + "String2"; }
	public string test_string_3 { public get; private set; default = "String1" + "String2" + "String3"; }

	const MyStruct[] my_struct = {
		{MY_DATA_PATH + "File1", 1},
		{MY_DATA_PATH + "File2", 2},
		{MY_DATA_PATH + "File3",3}
	};
}

public static void main(string[] argv)
{
	string test = "a" + "b";
	assert (test == "ab");
	
	assert (test_a == "Item A + Item B");
	
	string test_ab = test_a + " + " + test_b;
	assert (test_ab == "Item A + Item B + Item C");
	
	assert (FunWithConstants.test_c == "Item D + Item E");
	
	var t = new FunWithConstants ();

	assert (t.test == "String1String2String3");
	test = t.test_string_2 + "AhAh";
	assert (test == "String1String2AhAh");
	assert (t.test_string_2 == "String1String2");
	assert (t.test_string_3 == "String1String2String3");
}

