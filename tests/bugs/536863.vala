using GLib;

public class Foo : Object {
        public int prop {
                get; set;
        }
        
        public string get_name () {
                return "Foo";
        }
}

public interface Foobar {
        public abstract int prop {
                get; set;
        }
        
        public abstract string get_name ();
}


public class Bar : Foo, Foobar {

        public string another_property {
                get; set;
        }

}

public static void main ( string[] args ) {
        Bar b = new Bar ();
        b.prop = 3;
        assert(b.prop == 3);
        assert(b.get_name () == "Foo");
}

