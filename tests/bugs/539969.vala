using GLib;
using Gee;

public class Number : GLib.Object {
	
	public int Value {get; set;}
	
	public Number (int val) {
		this.Value = val;
	}
	
	 public static int compare (Number* n1, Number* n2) {
	 	if (n1->Value > n2->Value) return -1;
	 	else if (n1->Value < n2->Value) return 1;
	 	else return 0;
	 }
}

public class Test : GLib.Object {

	public HashMap<int, SequenceIter<Number>> event_id_map;
	public Sequence<Number> numbers;
        
    construct {
        this.event_id_map = new HashMap<int, SequenceIter<Number>> ();
        this.numbers = new Sequence<Number> (null);
    }
    
    public void add (owned Number n) {
    	SequenceIter<Number> iter = this.numbers.insert_sorted (n, Number.compare);
		this.event_id_map.set (n.Value, iter);
		
		assert (this.event_id_map.size == this.numbers.get_length ());
    }

	public static void main (string[] args) {
		Test t = new Test ();
		
		Number[] numbers = new Number[7];
		numbers[0] = new Number (-1);
		numbers[1] = new Number (1);
		numbers[2] = new Number (3);
		numbers[3] = new Number (5);
		numbers[4] = new Number (-1274);
		numbers[5] = new Number (128359);
		numbers[6] = new Number (0);
		
		t.add ((owned)numbers[0]);
		t.add ((owned)numbers[1]);
		t.add ((owned)numbers[2]);
		t.add ((owned)numbers[3]);
		t.add ((owned)numbers[4]);
		t.add ((owned)numbers[5]);
		t.add ((owned)numbers[6]);
		
		assert (t.event_id_map.size == 7);
		assert (t.numbers.get_length () == 7);
		foreach (int i in t.event_id_map.keys) {
			SequenceIter<Number> iter = t.event_id_map.get (i);
			Number n = t.numbers.get (iter);
			assert (n.Value == i);
		}
	}
}

