using GLib;

public class Test:GLib.Object {

        public Time date1 { get; set; }
        public Time date2 { get; set; }
        
        public Test (Time date) {
                this.date2 = date;
        }

        public static void main () {
                Time date = Time.gm (time_t ());
                Test t = new Test (date);

                t.date1 = Time.local (time_t ());
        }
}

