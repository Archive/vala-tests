public class Foo : GLib.Object {
	public string bar_str {
		owned get {
			try {
				return method_which_throws_an_error ();
			} catch (GLib.Error e) {
				GLib.error ("Error: %s", e.message);
			}
		}
	}
}

private string method_which_throws_an_error () throws GLib.Error {
	return "Hello";
}

private static int main (string[] args) {
	var foo = new Foo ();
	GLib.debug (foo.bar_str);

	return 0;
}

