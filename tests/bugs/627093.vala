public struct MyStruct
{
    public int[] array;
}

struct MyStruct2
{
    public int[] array;
}

int main ()
{
    MyStruct test = MyStruct ();
    test.array += 1;
    test.array += 2;

    stdout.printf ("%d\n", test.array[1]);

    MyStruct2 test2 = MyStruct2 ();
    test2.array += 1;
    test2.array += 2;

    stdout.printf ("%d\n", test2.array[1]);


    return 0;
}
