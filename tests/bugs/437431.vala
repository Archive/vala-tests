using GLib;

public delegate void myCallback();

public class SignalTest3: Object {

	public signal void foo();

	private void foo_helper() {
		foo();
	}

	private void hello() {
		stdout.printf("hello ");
	}

	private void world() {
		stdout.printf("world!\n");
	}

	public void caller(myCallback cb) {
		cb();
	}

	public void run() {
		foo += hello;
		foo += world;

		caller(foo);
		caller(foo_helper);
	}

	static int main(string[] args) {
		var st = new SignalTest3();
		st.run();

		return 0;
	}
}

