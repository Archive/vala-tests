public struct Struct
{
        public string a;
        public int b;
}

public Struct[] createArray()
{
        var array = new Struct[] {};
        array += Struct() { a = "Hello", b = 10 };
        array += Struct() { a = "World", b = 42 };
        return array;
}

public class Klass
{
        public Struct[] arrayOfStruct { get; set; }
        public void run()
        {
                arrayOfStruct = createArray();
        }
}

void main()
{
        var obj = new Klass();
        obj.run();
        assert( obj.arrayOfStruct != null );
        assert( obj.arrayOfStruct[0].a == "Hello" );
        assert( obj.arrayOfStruct[1].a == "World" );
}
