using GLib;

void main () {
	var test = new int[10];
	var z = test[(char)0];
	z = test[(uchar)0];
	z = test[(int)0];
	z = test[(uint)0];
	z = test[(short)0];
	z = test[(long)0];
	z = test[(ulong)0];
	z = test[(size_t)0];
	z = test[(ssize_t)0];
	z = test[(int8)0];
	z = test[(uint8)0];
	z = test[(int16)0];
	z = test[(uint16)0];
	z = test[(int32)0];
	z = test[(uint32)0];
	z = test[(int64)0];
	z = test[(uint64)0];
}