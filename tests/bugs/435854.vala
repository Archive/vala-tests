using GLib;

public interface Something {
	public abstract void do_something_else(out Error err);

	public void do_something(out Error err) {
		/* should have used 'err' here but used 'error' */
		do_something_else(ref error);
	}
}

