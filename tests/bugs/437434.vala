class Test: Object {
	[Description (nick = "foo", blurb = "bar")]
	public string test { get; set; default = "baz"; }

	public static int main () {
		var t = new Test ();

		assert (t.test == "baz");

		var spec = ((ObjectClass)typeof (Test).class_peek ()).find_property ("test");
		// var spec = (typeof (Test).class_peek () as ObjectClass).find_property ("test");

		return (spec.get_blurb () == "bar" && spec.get_nick () == "foo") ? 0 : 1;
	}
}
