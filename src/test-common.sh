#!/bin/bash

function _test_get_args {
    while true ; do
        case "$1" in
	    --interact)
                INTERACT=1 ;
                shift ;;
            --memcheck)
                VALGRIND="valgrind" ;
                shift ;;
            --massif)
                VALGRIND="valgrind" ;
                VALGRINDFLAGS="--tool=massif --massif-out-file=$TESTNAME.massif -q" ;
                shift ;;
	    --) shift ; break ;;
	    *) break ;;
        esac
    done
}

# hackish
function _test_make_guesses {
    ! grep "main" $TESTSRC >/dev/null && TESTRUN="${TESTRUN:-no}" VALAFLAGS="$VALAFLAGS -C"
    grep "Gtk" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gtk+-2.0" TESTRUN="${TESTRUN:-interact}"
    grep -i "gio" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gio-2.0"
    grep "DBus\." $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg dbus-glib-1" TESTRUN="${TESTRUN:-skip}"
    grep -i "cairo" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg cairo" TESTRUN="${TESTRUN:-interact}"
    grep "GConf\." $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gconf-2.0" TESTRUN="${TESTRUN:-skip}"
    grep "Glade\." $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg libglade-2.0"
    grep "Module\." $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gmodule-2.0"
    grep "Gnome\.Desktop" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gnome-desktop-2.0"
    grep "GMenu\." $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg libgnome-menu" VALAFLAGS="$VALAFLAGS -X -DGMENU_I_KNOW_THIS_IS_UNSTABLE=1"
    grep "Gst\." $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gstreamer-0.10"
    grep "x11_drawable_get_xid" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gdk-x11-2.0" TESTRUN="${TESTRUN:-interact}"
    grep "XOverlay" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gstreamer-interfaces-0.10"
    grep "using Panel" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg libpanelapplet-2.0" TESTRUN="${TESTRUN:-interact}"
    grep -i "pango" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg pangocairo"
    grep "Poppler\." $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg poppler-glib"
    grep "SocketConnection" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gnio"
    grep "WebKit" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg webkit-1.0" TESTRUN="${TESTRUN:-interact}"
    grep "Gee" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gee-1.0"
    grep "Xml" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg libxml-2.0"
    grep "Clutter\." $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg clutter-1.0" TESTRUN="${TESTRUN:-interact}"
    grep "Curses" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg curses -X -lncurses" TESTRUN="${TESTRUN:-interact}"
    grep "Soup" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg libsoup-2.4"
    grep "Json" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg json-glib-1.0"
    grep "using SDL;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg sdl"
    grep "using SDLGraphics;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg sdl-gfx"
    grep "CouchDB" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg couchdb-glib-1.0 --pkg json-glib-1.0"
    grep "Sqlite" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg sqlite3"
    grep "Posix" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg posix"
    grep "Hildon" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg hildon-1"
    grep "Readline" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg readline" VALAFLAGS="$VALAFLAGS -X -lreadline"
    grep "DesktopAppInfo" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gio-unix-2.0"
    grep "Gsl" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gsl" VALAFLAGS="$VALAFLAGS -X -lgsl -X -lgslcblas -X -lm"
    grep "ZLib" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg zlib -X -lz"
    grep "Lua" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg lua -X -llua -X -ldl -X -lm"
    grep "using Lm" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg loudmouth-1.0"
    grep "using GL;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gl"
    grep "using GLU;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg glu"
    grep "using GLUT;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg glut" TESTRUN="${TESTRUN:-interact}"
    grep "using GLX;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg glx"
    grep "using GLFW;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg libglfw" TESTRUN="${TESTRUN:-interact}"
    grep "using Tiff;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg tiff" VALAFLAGS="$VALAFLAGS -X -ltiff"
    grep "using Postgres;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg libpq"
    grep "using Gdl;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gdl-1.0"
    grep "GLDrawable" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg gtkglext-1.0"
    grep "using X;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg x11"
    grep "using Mx;" $TESTSRC >/dev/null && PACKAGES="$PACKAGES --pkg mx-1.0"
}

function _test_setup() {

    TESTNAME=`basename $0 .test`
    SRCDIR=`dirname $0`
    TESTSRC="${TESTSRC:-$SRCDIR/$TESTNAME.vala}"
    _test_make_guesses
    TESTRUN="${TESTRUN:-yes}"
    TESTENV="${TESTENV:-G_DEBUG=fatal_warnings}"
    TESTEXIT="${TESTEXIT:-0}"
    if [ "$TESTEXIT" -eq 0 ]; then
        TESTV="${TESTV:-1}"
    else
        TESTV="${TESTV:-0}"
    fi
    TESTTIMEOUT="${TESTTIMEOUT:-10s}"
    [ "$TESTNAME" -gt 0 2>/dev/null ] && BUGID="${BUGID:-$TESTNAME}"
    [ -n "$BUGID" ] && BUGZILLA="http://bugzilla.gnome.org/show_bug.cgi?id=$BUGID"

    VALAC="${VALAC:-valac}"
    VALACEXIT="${VALACEXIT:-0}"
    VALAFLAGS="${VALAFLAGS:-}"
    if [ "$VALACEXIT" -eq 0 ]; then
        VALAV="${VALAV:-1}"
    else
        VALAV="${VALAV:-0}"
    fi

    VALGRINDENV="${VALGRINDENV:-G_SLICE=always-malloc G_DEBUG=gc-friendly GLIBCPP_FORCE_NEW=1 GLIBCXX_FORCE_NEW=1}"
    VALGRINDFLAGS="${VALGRINDFLAGS:---tool=memcheck --leak-check=full --error-exitcode=1 -q}"

    _test_get_args "$@"

    STDOUT=`mktemp`
    STDERR=`mktemp`

    trap _test_trap SIGHUP SIGINT SIGTERM

    type test_setup >/dev/null 2>&1 && test_setup
}

function _test_trap {
    _test_teardown
    exit $exitval
}

function _test_compile_check {
    # discard the test run if it didn't produce an executable
    if [ "$exitval" != 0 ]; then
        TESTRUN=
    fi

    if [ "$exitval" -ne "$VALACEXIT" ]; then
        echo ""
        echo "*** WARNING: valac was expecting to return $VALACEXIT (got $exitval) ***"
        [ -n "$BUGZILLA" ] && echo "*** Original bug: $BUGZILLA"
        echo ""
    else
        exitval=0
    fi

    if type test_compile_check >/dev/null 2>&1; then
        test_compile_check || test_fail
    fi

    if [ "$VALAV" = 1 ]; then
        cat $STDOUT
        cat $STDERR
    fi
}

function _test_compile {
    VALAFLAGS="$VALAFLAGS -X -ggdb -X -O0"
    if [ -n "$CFLAGS" ]; then
       VALAFLAGS="$VALAFLAGS -X '$(echo $CFLAGS)'"
    fi
    if [ -n "$LDFLAGS" ]; then
       VALAFLAGS="-X '$(echo $LDFLAGS)' $VALAFLAGS"
    fi
    compile="$VALAC $VALAFLAGS $PACKAGES -o $TESTNAME $TESTSRC"
    echo "$compile"
    timeout ${TESTTIMEOUT} sh -c "G_DEBUG=fatal_warnings $compile" >$STDOUT 2>$STDERR
    exitval="$?"
    _test_compile_check
}

function _test_run_check {
    if [ "$exitval" -ne "$TESTEXIT" ]; then
        echo ""
        echo "*** WARNING: $TESTNAME was expecting to return $TESTEXIT (got $exitval) ***"
        echo ""
        [ -n "$BUGZILLA" ] && echo "original bug: $BUGZILLA"
    else
        exitval=0
    fi

    if type test_run_check >/dev/null 2>&1; then
        test_run_check || test_fail
    fi

    if [ "$TESTV" = 1 ]; then
        cat $STDOUT
        cat $STDERR
    fi
}

function _test_run {
    [ -n "$VALGRIND" ] && TESTENV="$TESTENV $VALGRINDENV" VALGRIND="$VALGRIND $VALGRINDFLAGS"

    if [ -z "$TESTRUN" -o "x$TESTRUN" = "xno" -o "x$TESTRUN" = "xskip" ]; then
        # skip
        true
    elif [ -z "$INTERACT" -a "x$TESTRUN" = "xinteract" ]; then
        echo "*** WARNING: This vala test is interactive and will not be run (try --interact) ***"
    else
        timeout ${TESTTIMEOUT} sh -c "$TESTENV $VALGRIND ./$TESTNAME $TESTARGS $@" >$STDOUT 2>$STDERR
        exitval="$?"
        _test_run_check
    fi
}

function _test_teardown {
    rm -f $STDOUT $STDERR $SRCDIR/$TESTNAME{,.c,.h}
    type test_teardown >/dev/null 2>&1 && test_teardown
}

function test_fail {
    _test_teardown
    exit 1
}

_test_setup "$@"
_test_compile
_test_run
_test_teardown
exit $exitval
