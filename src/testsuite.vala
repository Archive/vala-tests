/* testsuite.vala
 *
 * Copyright (C) 2008  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

public class Vala.TestSuite : Object {
	List<TestCase> tests;

	public void add_directory (string dirpath, string testpath = "/") {
		try {
			var dir = Dir.open (dirpath);
			string file;
			while ((file = dir.read_name ()) != null) {
				string filepath = Path.build_filename (dirpath, file);
				if (FileUtils.test (filepath, FileTest.IS_DIR)) {
					add_directory (filepath, testpath + file + "/");
				} else if (file.has_suffix (".test") && FileUtils.test (filepath, FileTest.IS_EXECUTABLE)) {
					var test = new TestCase (testpath + file.substring (0, file.length - ".test".length));
					tests.append (test);
					Test.add_data_func (test.path, test.run);
				}
			}
		} catch (FileError e) {
			warning ("Unable to open directory `%s'", dirpath);
		}
	}

	static void main (string[] args) {
		Test.init (ref args);

		var suite = new TestSuite ();
		suite.add_directory ("tests");

		Test.run ();
	}
}

