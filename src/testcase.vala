/* testcase.vala
 *
 * Copyright (C) 2008  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

public class Vala.TestCase : Object {
	public string path { get; set; }

	public TestCase (string path) {
		this.path = path;
	}

	public void run () {
		// prepare test dir
		string testdir = Path.build_filename (Environment.get_current_dir (), "test");

		try {
			Process.spawn_command_line_sync ("rm -rf \"" + testdir + "\"");
		} catch (SpawnError e) {
			error ("Failed to cleanup test directory: %s", e.message);
		}

		DirUtils.create (testdir, 0775);

		string testcase = Path.build_filename (Environment.get_current_dir (), "tests" + path + ".test");

		// run test case
		string[] args = new string[] { "/bin/sh", "-c", Shell.quote (testcase) + " 2>&1" };
		int exit_status;
		string output;

		try {
			Process.spawn_sync (testdir, args, null, 0, null, out output, null, out exit_status);
		} catch (SpawnError e) {
			error ("Failed to run test case: %s", e.message);
		}

		// cleanup
		try {
			Process.spawn_command_line_sync ("rm -rf \"" + testdir + "\"");
		} catch (SpawnError e) {
			error ("Failed to cleanup test directory: %s", e.message);
		}

		// report error
		if (exit_status != 0) {
			string format = "\n%s";
			error (format, output);
		}
	}
}

